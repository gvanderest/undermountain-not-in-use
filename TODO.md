# TODO List

## In Progress
* Channel Communication System
* Idle connection cleanup
* Flat-File Database Collections for Areas, Rooms, Actors, Objects, etc.
* Organizations (Clans, Factions, etc.)
* Scripting Engine for Areas, Rooms, Actors, Objects, etc.
* Combat in PVE and PVP forms
* Maps and Mapping
* Import from ROT - Waterdeep Specific at this time
* Equipment Slot and Weaponry
* Races
* Classes
* Genders
* Skills and Spells
* Character Creation
* Rerolling
* In-Depth Equipment Stat System
* Vehicles and Mounts
* Pets
* Experience and Levelling
* Shops
* Food and Drink with Effects
* Pills and Potions with Effects
* Staves and Wands and Scrolls with Effects

## Future Features
* Account System
* Henchmen
* Multi-Classing Support
* Specialization Support
* Seasonal Reset Systems
* Leaderboards for Time-Trials, Arena Victories, Player-Killing, etc.
* Random (Endless?) Dungeon Generators
* Player Housing
* Tradeskills
* Crafting System
* Raiding / Claiming / Encounter systems
* Instancing of Areas for Groups
* PVP Battlegrounds and Arenas
* Forum and Discussion System
* Global Effects and Buffs
* Support Ticket Submission System
* API Integrations
* Random (Bandit?) Encounters
* Pet Battles
* Minigame Support
* DDOS Protection
* Immortal Punishment and Control Systems
* Justice System
* Questing System
* Achievement System and Brackets
* Building Tools
* Auctioning and Marketplace System
* Forget System
* Friends System
* Grouping System
* Heal- and Damage-Over-Time Effects
* Global Roleplay Integrations
* Organization Builder Commands
* Clan Commands
* Mortal Administrator System
* Mail System
* Banking
* Garrison-Like Quests
* Detect Enemies-like Skills for Minimap
* Tutorial Area for "Adventurer" type of class
* Tracking of failed login attempts
