"""
INJECTOR
"""


class Injector(object):
    """A class that binds to a Game."""
    def __init__(self, game):
        self.game = game
        self.init()

    def init(self):
        pass
